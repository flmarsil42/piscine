int		ft_iterative_power(int nb, int power)
{
	int	result;

	result = 1;
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	if (power == 1)
		return (nb);
	if (nb == 0)
		return (0);
	while (power >= 1)
	{
		result = result * nb;
		power--;
	}
	return (result);
}
