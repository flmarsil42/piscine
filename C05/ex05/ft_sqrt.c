int		ft_sqrt(int nb)
{
	long	i;
	long	num;

	i = 1;
	num = nb;
	if (num < 0)
		return (0);
	while (i * i < num)
		i++;
	if (i * i == num)
		return (i);
	return (0);
}
