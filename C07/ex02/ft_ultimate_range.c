#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int *tab;
	int size;

	i = 0;
	tab = NULL;
	size = (max - min);
	if (min >= max)
	{
		*range = 0;
		return (0);
	}
	if (!(tab = malloc(sizeof(int) * size)))
		return (-1);
	while (i < size)
	{
		tab[i] = min + i;
		i++;
	}
	*range = tab;
	return (size);
}
