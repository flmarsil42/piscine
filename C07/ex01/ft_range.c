#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int size;
	int *tab;
	int i;

	i = 0;
	size = (max - min);
	tab = NULL;
	if (min >= max)
		return (NULL);
	if (!(tab = malloc(sizeof(int) * size)))
		return (NULL);
	while (min < max)
	{
		tab[i] = min;
		min++;
		i++;
	}
	return (tab);
}
