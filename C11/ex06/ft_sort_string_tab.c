void	ft_swap(char **a, char **b)
{
	char *c;

	c = *a;
	*a = *b;
	*b = c;
}

int		ft_strcmp(char *s1, char *s2)
{
	unsigned int i;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		i++;
	}
	if ((s1[i] == '\0' && s2[i] == '\0') || (s2[i] == '\0' && s1[i] != '\0'))
		return (s1[i] - s2[i]);
	return (0);
}

void	ft_sort_string_tab(char **tab)
{
	int x;
	int i;

	i = 0;
	while (tab[i])
	{
		x = 0;
		while (tab[x])
		{
			if (tab[x + 1] && ft_strcmp(tab[x], tab[x + 1]) > 0)
				ft_swap(&tab[x], &tab[x + 1]);
			x++;
		}
		i++;
	}
}
