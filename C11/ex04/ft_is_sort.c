int	ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int	i;
	int	j;
	int	n;

	i = -1;
	n = 1;
	while (n && ++i < length)
	{
		j = i;
		while (n && ++j < length)
			n = ((f(tab[i], tab[j])) <= 0) ? n : 0;
	}
	if (n)
		return (n);
	++n;
	i = -1;
	while (n && ++i < length)
	{
		j = i;
		while (n && ++j < length)
			n = ((f(tab[i], tab[j])) >= 0) ? n : 0;
	}
	return ((n > 0));
}
